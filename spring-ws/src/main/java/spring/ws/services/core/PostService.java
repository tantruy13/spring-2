package spring.ws.services.core;

import spring.base.ObjectResponseModel;
import spring.models.PostModel;

/**
 * Created by Tan on 4/6/2017.
 */
public interface PostService {
    ObjectResponseModel<PostModel> getPost(long id);
}
