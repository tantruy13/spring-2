package spring.ws.services.impl;

import spring.constants.RestError;
import spring.entities.Post;
import spring.base.ObjectResponseModel;
import spring.models.PostModel;
import spring.repos.PostRepo;
import spring.ws.services.core.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static spring.builder.PostModelBuilder.aPostModel;
/**
 * Created by Tan on 4/6/2017.
 */
@Service
public class PostServiceImpl implements PostService {
    private static final Logger logger = LoggerFactory.getLogger(PostServiceImpl.class);
    @Autowired
    private PostRepo postRepo;

    @Override
    public ObjectResponseModel<PostModel> getPost(long id) {
        ObjectResponseModel<PostModel> postModel = new ObjectResponseModel<>();
        try {
            Post one = postRepo.findOne(id);
            Objects.requireNonNull(one);
            postModel.setContent(toPostModel(one));
        } catch (NullPointerException e) {
            postModel.setDesc("null : "+e.getMessage());

            postModel.setErrorCode(RestError.ERROR_DB);
        } catch (Exception e) {
            logger.error("getPost", e);
            postModel.setDesc(e.getMessage());
            postModel.setErrorCode(RestError.ERROR_DB);
        }
        return postModel;
    }

    private PostModel toPostModel(Post post) {
        return aPostModel()
                .withId(post.id)
                .withContent(post.content)
               .build();
    }
}
