package spring.ws.controllers;

import spring.ws.models.Person;
import spring.ws.services.core.PostService;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 * Created by Tan on 4/6/2017.
 */
@Path("/")
public class RestDemo {
    static final Logger logger = LoggerFactory.getLogger(RestDemo.class);
    @Autowired
    private PostService postService;

    @GET
    @Path("/ok")
    @Produces(MediaType.TEXT_PLAIN)
    public Response check() {
        return Response.ok("OK").build();
    }

    @GET
    @Path("/person01")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPerson01() {
        return Response.ok(new Person(20, "Linh")).build();
    }

    @GET
    @Path("/person02")
    @Produces(MediaType.APPLICATION_JSON)
    public Person getPerson02() {
        return new Person(20, "Linh");
    }

    @POST
    @Path("/postPerson01")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response consumePerson(Person person) {
        logger.info(ToStringBuilder.reflectionToString(person));
        return Response.ok("OK").build();
    }

    @GET
    @Path("/getPost/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPost(@PathParam("id") long id) {
        logger.debug("Getting PostModel for {}", id);
        return Response.ok(postService.getPost(id)).build();
    }
}
