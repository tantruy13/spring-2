package spring.entities;

/**
 *  Created by Tan on 4/6/2017.
 */
public enum PostState {
    PUBLISHED, DRAFT, DELETED;
}
