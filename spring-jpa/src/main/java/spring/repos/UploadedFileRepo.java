package spring.repos;

import org.springframework.data.repository.CrudRepository;
import spring.entities.UploadedFile;

/**
 * Created by Tan on 4/7/2017.
 */
public interface UploadedFileRepo extends CrudRepository<UploadedFile,Long> {
}
