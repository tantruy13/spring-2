package spring.repos;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import spring.entities.GroupWork;

/**
 * Created by Tan on 4/7/2017.
 */
public interface GroupWorkRepo extends CrudRepository<GroupWork,Long> {
//    @Query("SELECT gw FROM GroupWork WHERE Groupwork.id = ")
//    @Transactional
//    public void getByID(Long id);
}
