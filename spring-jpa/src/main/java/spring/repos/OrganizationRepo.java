package spring.repos;

import org.springframework.data.repository.CrudRepository;
import spring.entities.Organization;

/**
 * Created by Tan on 4/7/2017.
 */
public interface OrganizationRepo extends CrudRepository<Organization,Long> {
}
