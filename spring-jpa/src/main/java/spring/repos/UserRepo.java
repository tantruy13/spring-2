package spring.repos;

import org.omg.CORBA.StructMember;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import spring.entities.User;

/**
 * Created by Tan on 4/7/2017.
 */
@Repository
public interface UserRepo extends CrudRepository<User, String> {
}
