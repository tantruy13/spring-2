<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title><sitemesh:write property='title' /></title>
    <spring:url value="/resources/bootstrap/css/bootstrap.min.css" var="boostrapCss" />
    <link href="${boostrapCss}" rel="stylesheet">

    <spring:url value="/resources/jquery.dataTables.min.css" var="jtableCss" />
    <link href="${jtableCss}" rel="stylesheet">

    <spring:url value="/resources/jquery.min.js" var="jqueryJs" />
    <script src="${jqueryJs}"></script>
    <spring:url value="/resources/mycustom.js" var="customJs" />
    <script src="${customJs}"></script>
    <spring:url value="/resources/bootstrap/js/bootstrap.min.js" var="bootstrapJs" />
    <script src="${bootstrapJs}"></script>

    <spring:url value="/resources/jquery.dataTables.min.js" var="datatableJs" />
    <script src="${datatableJs}"></script>

    <spring:url value="/resources/angularjs/angularjs.min.js" var="angularjsMin" />
    <script src="${angularjsMin}"></script>

    <spring:url value="/resources/angularjs/myAngularjs.js" var="myAngularjsMin" />
    <script src="${myAngularjsMin}"></script>

    <sitemesh:write property='head' />

  </head>

  <body ng-app="geek-app">

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="#">Home</a></li>
            <li role="presentation"><a href="#">About</a></li>
            <li role="presentation"><a href="#">Contact</a></li>
          </ul>
        </nav>
        <h3 class="text-muted">Project name</h3>
      </div>

      <sitemesh:write property='body' />

      </div>

      <footer class="footer">
        <p>&copy; 2016 Company, Inc.</p>
      </footer>

    </div> <!-- /container -->

  </body>
</html>
